package com.example.flowers;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.o_nas:
                Intent intent=new Intent(MainActivity.this, AboutUs.class);
                startActivity(intent);
                break;

            case R.id.cveti:
                Intent intent2=new Intent(MainActivity.this, OurWorks.class);
                startActivity(intent2);
                break;

            case R.id.login:
                Intent intent3=new Intent(MainActivity.this, Login.class);
                startActivity(intent3);
                break;

            case R.id.zvonok:
                Intent intent4=new Intent(Intent.ACTION_CALL);
                intent4.setData(Uri.parse("tel:+37495500121"));
                if(ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE) !=
                        PackageManager.PERMISSION_GRANTED){
                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{Manifest.permission.CALL_PHONE}, 1);
                }else startActivity(intent4);
                break;

            case R.id.mergorcer:
                Intent intent5=new Intent(MainActivity.this, OurWorks.class);
                startActivity(intent5);
                break;

            case R.id.facebook:
                Intent intent6=new Intent(MainActivity.this, Facebook.class);
                startActivity(intent6);
                break;
            case R.id.map:
                Intent intent7=new Intent(Intent.ACTION_VIEW);
                intent7.setData(Uri.parse("geo:40, 44"));
                Intent intent8=Intent.createChooser(intent7, "map");
                startActivity(intent8);
                break;
            case R.id.kap:
                Intent intent9=new Intent(MainActivity.this, Kap.class);
                startActivity(intent9);
                break;
        }
    }
}