package com.example.flowers;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {
    EditText etusername, etpassword, etname, etsurname, etemail, etregpassword, etnewpassword;
    Button login, signin;
    DbHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        dbHelper=new DbHelper(this);

        etusername=findViewById(R.id.username);
        etpassword=findViewById(R.id.password);

        etname=findViewById(R.id.name);
        etsurname=findViewById(R.id.surname);
        etemail=findViewById(R.id.email);
        etregpassword=findViewById(R.id.regpassword);
        etnewpassword=findViewById(R.id.newpassword);


        login=findViewById(R.id.login);

        signin=findViewById(R.id.signin);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String username=etusername.getText().toString();
                String password=etpassword.getText().toString();

                String searchpass=dbHelper.searchpassword(username);

                if(password.equals(searchpass)){
                    String nameSurname=dbHelper.nameAndSurname(username);
                    Intent intent=new Intent(Login.this, UserPage.class);
                    intent.putExtra("name", nameSurname);
                    startActivity(intent);
                }else{
                    Toast.makeText(Login.this, "Մուտքանունը կամ գաղտնաբառը սխալ է", Toast.LENGTH_SHORT).show();
                }
            }
        });

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name=etname.getText().toString();
                String surname=etsurname.getText().toString();
                String username=etemail.getText().toString();
                String regpassword=etregpassword.getText().toString();
                String confirmPasword=etnewpassword.getText().toString();

                if(!regpassword.equals(confirmPasword)){
                    Toast.makeText(Login.this, "Գաղտնաբառը չի համընկնում", Toast.LENGTH_SHORT).show();
                }else{
                    User user=new User();
                    user.setName(name);
                    user.setSurname(surname);
                    user.setUsername(username);
                    user.setRegpassword(regpassword);

                    dbHelper.insertUser(user);
                    Toast.makeText(Login.this, "Գրանցումը հաջողությամբ կատարվել է", Toast.LENGTH_SHORT).show();
                    etusername.setText(username);
                    etpassword.setText(regpassword);

                }
            }
        });


    }
}
