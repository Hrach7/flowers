package com.example.flowers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {

    private static final String DB_NAME="users.db";
    private static final String TABLE_NAME="users";
    private static final String ID="id";
    private static final String NAME="name";
    private static final String SURNAME="surname";
    private static final String USERNAME="username";
    private static final String PASSWORD="password";
    SQLiteDatabase db;

    public DbHelper(Context context){
        super(context, DB_NAME, null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table users(id integer primary key Autoincrement, name Varchar(20) not null, surname Varchar(20) not null," +
                "username Varchar(20) not null, password Varchar(20) not null)");
        this.db=db;

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String query =" DROP TABLE IF EXISTS "+TABLE_NAME;
        db.execSQL(query);
        this.onCreate(db);

    }

    public void insertUser(User user){
        db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(NAME, user.getName());
        contentValues.put(SURNAME, user.getSurname());
        contentValues.put(USERNAME, user.getUsername());
        contentValues.put(PASSWORD,user.getRegpassword());

        db.insert(TABLE_NAME, null, contentValues);
        db.close();
    }
    public String searchpassword(String username){
        db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery("select username, password  from users", null);
        String uname, pass;
        pass="not found";
        if(cursor.moveToFirst()){
            do{
                uname=cursor.getString(0);

                if(uname.equals(username)){
                    pass=cursor.getString(1);


                    break;
                }
            }
            while (cursor.moveToNext());
        }
        return pass;

    }

    public String nameAndSurname(String username){
        db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery("select username, name, surname from users", null);
        String uname, nameSurname, name, surname;

        nameSurname="";
        if(cursor.moveToFirst()){
            do{
                uname=cursor.getString(0);

                if(uname.equals(username)){
                    name=cursor.getString(1);
                    surname=cursor.getString(2);
                    nameSurname=name+" "+surname;
                    break;
                }
            }
            while (cursor.moveToNext());
        }
        return nameSurname;
    }

}

